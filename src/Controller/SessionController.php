<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;

class SessionController extends AbstractController
{
    /**
     * @Route("/sessions/creer/variables/session")
     */
    public function creerVariableSession(SessionInterface $objSession)
    {
        // avant
        // session_start
        // $SESSION['nom'] = 'Lucy';
        // session_close

        // la session peut contenir n'importe quelle structure de données
        $objSession->set('nom', 'Lucy');

        return $this->render('session/creer_variables_session.html.twig');
    }

    /**
     * @Route("/sessions/afficher/variables/session")
     */
    public function afficherVariableSession(SessionInterface $objSession)
    {
        // avant
        // session_start
        // $SESSION['nom'] = 'Lucy';
        // session_close

        // la session peut contenir n'importe quelle structure de données
        $nom = $objSession->get('nom');

        return $this->render('session/afficher_variables_session.html.twig', [
            'nom' => $nom
        ]);
    }
}
