<?php

namespace App\Controller;

use Doctrine\Common\Collections\Expr\Value;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\Routing\Annotation\Route;

class ChuckNorrisAPIController extends AbstractController
{
    /**
     * @Route("/chuck/norris/affiche_vue1")
     */
    public function afficheVue1()
    {
        return $this->render('chuck_norris_api/affiche_vue1.html.twig', []);
    }


    /**
     * @Route("/chuck/norris/affiche_vue2")
     */
    public function afficheVue2()
    {
        return $this->render('chuck_norris_api/affiche_vue2.html.twig');
    }


    public function afficheFromApi($nbBlagues)
    {
        $client = HttpClient::create();

        $bla = ["aaa", "dfdf", "hhh"];

        $blagues = [];

        for ($i = 0; $i < $nbBlagues; $i++) {
            $response = $client->request('GET', 'https://api.chucknorris.io/jokes/random');
            $contents = $response->getContent();
            $blagues[] = json_decode($contents)->value;
        }

        // dump(json_decode($contents)->value);
        // die();

        return $this->render('chuck_norris_api/affiche_api.html.twig', [
            // 'blagues' => $bla,
            'blagues' => $blagues,
        ]);
    }
}
