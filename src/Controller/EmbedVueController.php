<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class EmbedVueController extends AbstractController
{
    /**
     * @Route("/embed/controller/vue/affiche/vue1")
     */
    public function afficheVue1()
    {
        return $this->render('embed_vue/affiche_vue1.html.twig');
    }

    /**
     * @Route("/embed/controller/vue/affiche/vue2")
     */
    public function afficheVue2()
    {
        return $this->render('embed_vue/affiche_vue2.html.twig');
    }

    // appelle à methode sans route
    // on peut donc mettre directement les parametres dans les parenthèses de la méthode
    public function donnesDynamique($nombreVilles)
    {
        $villes = ["Buxelles", "Kigali", "Colmar"];
        $villes = array_slice($villes, 0, $nombreVilles);

        return $this->render('embed_vue/donnesDynamiques.html.twig', [
            'villes' => $villes
        ]);
    }
}
