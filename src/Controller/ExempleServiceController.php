<?php

namespace App\Controller;

use App\Services\Bonjour;
use App\Services\Chuky;
use App\Services\ChukyLogger;
use App\Services\Irail;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class ExempleServiceController extends AbstractController
{
    // avec les services, on n'instancie pas un nouvel objet à chaque fois
    // il y a un seul objet de ce type/ ce service est commun à tous les appleants


    /**
     * @Route("/exemple/service", name="exemple_service")
     */
    public function utiliseLogger(LoggerInterface $monLogger)
    {
        // s'utilise avec un try catch normalement
        // pour les error apparaissent uniquement lsq il y a effectivemet une erreur
        $monLogger->info("J ai faim");
        return $this->render('exemple_service/utilise_logger.html.twig');
    }

     /**
     * @Route("/exemple/bonjour")
     */
    public function utiliseBonjour(Bonjour $objetBonjour)
    {
        $hello = $objetBonjour->direBonjour();

        return $this->render('exemple_service/utilise_bonjour.html.twig', [
            'hello' => $hello
        ]);
    }


    /**
     * @Route("/exemple/chuky/{homany}")
     */
    public function utiliseChuky(Chuky $myChuky, Request $req)
    {
        $nbJokes = $req->get('homany');
        $myJokes = $myChuky->getPhrase($nbJokes);

        return $this->render('exemple_service/utilise_chuky.html.twig', [
            'jokes' => $myJokes,
        ]);
    }


    /**
     * @Route("/exemple/chukyLogger")
     */
    public function utiliseChukyLogger(ChukyLogger $mychukyLogger)
    {
        $myJoke = $mychukyLogger->getPhrase();

        return $this->render('exemple_service/utilise_chukyLogger.html.twig', [
            'joke' => $myJoke,
        ]);
    }

    /**
     * @Route("/exemple/irail")
     */
    public function utiliseIrail(Irail $myRail)
    {
        $json = $myRail->getLiveboard();

        // return $this->render();
    }
}
