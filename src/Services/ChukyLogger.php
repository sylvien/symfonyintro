<?php

namespace App\Services;

use Psr\Log\LoggerInterface;
use Symfony\Component\HttpClient\HttpClient;

class ChukyLogger{

    // pour avoir des services dans des services
    // il faut le spécifier au moyen d'un controller
    private $myLogger;
    public function __construct(LoggerInterface $logger)
    {
        $this->myLogger = $logger;
    }


    public function getPhrase()
    {
        $client = HttpClient::create();

        $response = $client->request('GET', 'https://api.chucknorris.io/jokes/random');
        $contents = $response->getContent();
        $this->myLogger->info("je suis l info du log dans le sevrice ChukyLogger");
        return json_decode($contents)->value;

    }
}
