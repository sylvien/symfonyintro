<?php

namespace App\Services;

use Symfony\Component\HttpClient\HttpClient;

class Chuky{

    public function getPhrase($combien)
    {
        $client = HttpClient::create();

        $jokes = [];

        for ($i=0; $i < $combien; $i++) { 
            $response = $client->request('GET', 'https://api.chucknorris.io/jokes/random');
            $contents = $response->getContent();
            $jokes[] = json_decode($contents)->value;
        }

        return $jokes;
    }
}
