<?php

namespace App\Services;

use Symfony\Component\HttpClient\HttpClient;

class Irail {

    public function getLiveboard()
    {
        $client = HttpClient::create();
        $response = $client->request('GET', 'http://api.irail.be:80/liveboard/?id=BE.NMBS.008812005&lang=fr&format=json');

        // dump($response); est un object
        // dump($response->getContent()); est un string json
        // die();

        return $response->toArray();

    }
}